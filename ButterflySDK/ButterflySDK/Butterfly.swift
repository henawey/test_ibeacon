//
//  Butterfly.swift
//  ButterflySDK
//
//  Created by Ahmed Henawey on 3/28/18.
//  Copyright © 2018 Ahmed Henawey. All rights reserved.
//

import CoreLocation
import UserNotifications
import FirebaseDatabase
import Firebase

public class Butterfly: NSObject {
    let locationManager = CLLocationManager()
//     major: 0x5154, minor: 0x0403
    var beaconRegion = CLBeaconRegion(proximityUUID: UUID(uuidString: "8d261c82-35f7-11e8-b467-0ed5f89f718b")!, identifier: "Butterfly")
    
    public override init() {
        super.init()
        FirebaseApp.configure()
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.allowsBackgroundLocationUpdates = true
        
        beaconRegion.notifyOnEntry = true
        beaconRegion.notifyOnExit = true
        beaconRegion.notifyEntryStateOnDisplay = true
        
        locationManager.startMonitoring(for: beaconRegion)
//        locationManager.requestState(for: beaconRegion)
        locationManager.startRangingBeacons(in: beaconRegion)
    }
}
// MARK: CLLocationManagerDelegate
extension Butterfly: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        Database.database().reference().child(Date().debugDescription).setValue("\(#function) \(#line) \(state.rawValue)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        Database.database().reference().child(Date().debugDescription).setValue("\(#function) \(#line)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Database.database().reference().child(Date().debugDescription).setValue("\(#function) \(#line) \(error)")
    }
    
    public func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        Database.database().reference().child(Date().debugDescription).setValue("\(#function) \(#line) \(error)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        Database.database().reference().child(Date().debugDescription).setValue("\(#function) \(#line) \((beacons.count > 0 ? beacons.first?.debugDescription : "No Beacon")!)")
    }
    
    public func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error) {
        
        Database.database().reference().child(Date().debugDescription).setValue("\(#function) \(#line) \(error)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        Database.database().reference().child(Date().debugDescription).setValue("\(#function) \(#line)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        Database.database().reference().child(Date().debugDescription).setValue("\(#function) \(#line)")
    }
}
