//
//  ButterflySDK.h
//  ButterflySDK
//
//  Created by Ahmed Henawey on 3/28/18.
//  Copyright © 2018 Ahmed Henawey. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ButterflySDK.
FOUNDATION_EXPORT double ButterflySDKVersionNumber;

//! Project version string for ButterflySDK.
FOUNDATION_EXPORT const unsigned char ButterflySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ButterflySDK/PublicHeader.h>

